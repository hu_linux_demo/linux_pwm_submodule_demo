/**
 * @file      : main.c
 * @brief     : 程序入口文件
 * @author    : huenrong (huenrong1028@gmail.com)
 * @date      : 2020-10-22 15:47:12
 *
 * @copyright : Copyright (c) 2020  胡恩荣
 *
 */

#include <stdio.h>
#include <stdint.h>

#include "./pwm/pwm.h"

// PWM定义
#define PWM_CHIP_0 0

int main(int argc, char *argv[])
{
    int ret = -1;

    // 导出设备
    ret = pwm_export(PWM_CHIP_0);
    if (0 != ret)
    {
        printf("export pwmchip%d fail\n", PWM_CHIP_0);

        return -1;
    }

    // 使能PWM
    ret = pwm_enable(PWM_CHIP_0);
    if (0 != ret)
    {
        printf("enable pwmchip%d fail\n", PWM_CHIP_0);

        return -1;
    }

    // 设置周期
    ret = pwm_set_period(PWM_CHIP_0, 50000);
    if (0 != ret)
    {
        printf("set pwmchip%d period fail\n", PWM_CHIP_0);

        return -1;
    }

    // 设置占空比
    ret = pwm_set_duty_cycle(PWM_CHIP_0, 10000);
    if (0 != ret)
    {
        printf("set pwmchip%d duty_cycle fail\n", PWM_CHIP_0);

        return -1;
    }

    // 取消导出
    ret = pwm_unexport(PWM_CHIP_0);
    if (0 != ret)
    {
        printf("unexport pwmchip%d fail\n", PWM_CHIP_0);

        return -1;
    }

    return 0;
}
